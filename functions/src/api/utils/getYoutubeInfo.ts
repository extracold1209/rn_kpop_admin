import fetch from 'isomorphic-unfetch';
import {ISong} from "../firebaseService";

const key = 'AIzaSyAA9seLWCTVrdZxLH5rtwCndqMb1xnGSDU';
const part = 'snippet,statistics';
const baseUrl = 'https://www.googleapis.com/youtube/v3/videos';

export default async function getYoutubeInfo(youtubeId: string): Promise<ISong> {
    const response = await fetch(`${baseUrl}?part=${part}&id=${youtubeId}&key=${key}`);

    const data = await response.json();
    const video = data.items[0];

    const { snippet, statistics } = video;

    const { publishedAt, title, thumbnails } = snippet;
    const { medium: defaultThumbnail } = thumbnails;
    const { url: thumbnailUrl } = defaultThumbnail;

    const { viewCount, likeCount } = statistics;

    return {
        name: title,
        youtubeId,
        youtubeViewCount: Number.parseInt(viewCount, 10),
        youtubePublishAt: publishedAt,
        clickCount: 0,
        playCount: 0,
        thumbnailUrl,
    };
}
