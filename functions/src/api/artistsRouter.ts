import express from "express";
import {body} from 'express-validator';
import {addArtists, getAllArtists, updateArtists} from "./firebaseService";

const router = express.Router();

router.get('/artists', async (req, res) => {
    const artists = await getAllArtists();
    res.json(artists);
});

router.post('/artists', body(['name', 'agency', 'members', 'debut', 'thumbnailImage']), async (req, res) => {
    await addArtists(req.body);
    res.send('ok');
});

router.put('/artists',
    body(['name', 'agency', 'members', 'debut', 'thumbnailImage', 'songs', 'docId']),
    async (req, res) => {
        await updateArtists(req.body);
        res.send('ok');
    }
);

export default router;
