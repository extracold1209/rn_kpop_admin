import * as admin from "firebase-admin";

export const firebaseApp = admin.initializeApp({
    credential: admin.credential.cert({
            "projectId": "rn-kpop",
            "clientEmail": "firebase-adminsdk-jajgr@rn-kpop.iam.gserviceaccount.com",
            "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDpa8KyTNnnlQbr\nx61c2vsqu90Pcu4/FGbQpZrceK6vQX0SnCEabREG0YsSGRQJVEfr2Vt7FZ9meM/u\nqRowNQsxndpwvrbbdglfATAQy5s3xkHGpBMGofroW/5lRvsq24rdLxPk85WIFGlW\nCewPkD4Kb2S+w3a5j20OHX1dlddOO/ioyh+rgboe1IrYZxqyBwlP+dEUglaCfpZa\nxrEdYlnV5j4sX6omG6KL9w+VhlOneIvNijJBmWkG1u0t/pMjkLIU8KKbClmeFK9x\nJuTjLjaNJ5N0AQn+J51keH8MZUa1RFm/GLCyh9MkBQ+plZJlHzGTJ3VM9h9IIg+a\nEHwmyLazAgMBAAECggEAZGbPLL1kvzl5bCKL3dRtU+DRwcroUqa2YMnS1kFstgKf\nx0jz7jd6qumn/aVe2nJvqzFnqYZP9ZZCrj/7fT/brEBfTL6jzbs0ZbKPl7HUdoWn\nFkfNzT0QPTEQuERzVehwDxqoXoMtzwYklvcWnE0peniWp31RDJj2SiuCGKaSoVnV\nGyv5vA0HHG7nYbo7SrJSSNIK8A0k8dfURNf0kzvrCTbBF45QD+F90uEEaTIqwrL/\nW+o2ablTJLkyhM1jAWt9CljrI4QSNgcwlxPttRYzx3p11ZaIHZp9gz9QgICgQIxI\nr+yllVoQSpX6ogRtiPXLXDCiN/7hPkGCeg+cLYiiyQKBgQD2MSiS1WLvVCaT8xAP\nTjdzhI1SfEZeD+gyg2Z+e+StoBPfwZ29eCJUJd46p8VbUxFh1oHVYz0dndQnJWoH\n/1KbkM48MKn1U6l9Y28d3lomd2YnZdgbTr6QmXMRYJpfu5HLstD9HzyyKSWJjktu\nEsf4EUPFO6izLdf8O5VheNvPCwKBgQDyuFp5tx0wFNe2mllv8JN2QO8gx0jC/XZT\nvjM6Ocbia6GEZ/47pk8E5lPnUjdND3ITHxu0AnvxxsFGvn/nO7RRqi5muGk6/f2Y\neu9jqL+V43zkGzvY2QZe11+b41Co6VrtJX3V6YGhKHDtutqNwx/GeAy9tOhMfA2F\n+cNLav4f+QKBgGc8sgDwzm4B4bOE56UjbYlZelGun2tuTDZyd5ZuqbRNPkhETrtX\nFz4mKwx5sf7b1JwscYFj/grfVkmcHNyBqshlbrY6yeqATMCx47SouzEKPKIkyPya\nhrroPiJ5WgLvCvJaZ9zdLnyfWIhYTTU5VvWld7/LhrCsdrfk3H1lSveFAoGAbv3w\nmbIrzb3qOt1wQqkftbbR4wpecpRXVMooQNsE4KtZS+QGNR7DCbmN6r0E/t8uSiIy\nl+l+5jv+TCx27rW1qpaI634u2MQnAGBPMiKLoa5KgUEW5/2uVRZl6TJUwzka1FEN\nrxE9rwWARqXopWTyFd717JZamAcuY/Kn5MgDd3kCgYAwh8NkKO99Z2SThlyE52sX\nRTrcVRxz1qd08GSjsk0xdwSMbhB0N3TB7Kx/fuZIA0BZMiUULTsUeqhnGBgYBhGE\nYDPDSktFNEPHtoTpWZ5GdqvBjQ3wPnS6c1U+oS/zq9EBgnA/0sIcmH/lVibH9rj7\nPUf1Uzk+pcU4FTeC3bnS6Q==\n-----END PRIVATE KEY-----\n",
        }
    ),
    databaseURL: "https://rn-kpop.firebaseio.com"
});

export const firestore = firebaseApp.firestore();
export const artistsCollection = firestore.collection('artists');
export const songsCollection = firestore.collection('songs');
