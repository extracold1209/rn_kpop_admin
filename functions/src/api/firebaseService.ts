import {convertArtistDocToJs, convertArtistJSToDoc} from "./converter";
import {artistsCollection} from "./firebaseInstances";

type Overwrite<T, U> = Pick<T, Exclude<keyof T, keyof U>> & U;

export type IArtistJs = {
    name: string;
    agency: string;
    members: string[];
    debut: string | FirebaseFirestore.Timestamp;
    twitterId?: string;
    vLiveId?: string;
    thumbnailImage: string;
    songs: ISong[] | FirebaseFirestore.DocumentReference[];
    docId?: string;
}

export type ISong = {
    // artistName: string;
    name: string;
    youtubeId: string;
    youtubeViewCount: number;
    youtubePublishAt: string | FirebaseFirestore.Timestamp;
    clickCount: number;
    playCount: number;
    thumbnailUrl: string;
    // genre: string[]
} | string;

export async function getAllArtists() {
    const snapshot = await artistsCollection.get();
    return await Promise.all(snapshot.docs.map(async (doc) => {
        const artist = await convertArtistDocToJs(doc);
        artist.docId = doc.id;
        return artist;
    }));
}

export async function addArtists(artistInfo: IArtistJs) {
    const artistDoc = await convertArtistJSToDoc(artistInfo);
    await artistsCollection.doc().set(artistDoc);
}

export async function updateArtists(artistInfo: IArtistJs) {
    if (!artistInfo.docId) {
        throw new Error('docId is not present');
    }

    const artistDoc = await convertArtistJSToDoc(artistInfo);
    await artistsCollection.doc(artistInfo.docId).set(artistDoc);
}
