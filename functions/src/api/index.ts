import express from 'express';
import artistRouter from './artistsRouter';

const app = express();
app.use(express.json());

app.use('/hello', async (req, res) => {
    res.send('aHello World from Firebase App!');
});
app.use(artistRouter);


export default app;
