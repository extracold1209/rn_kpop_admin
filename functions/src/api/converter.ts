import {IArtistJs, ISong} from "./firebaseService";
import * as admin from "firebase-admin";
import getYoutubeInfo from "./utils/getYoutubeInfo";
import {songsCollection} from "./firebaseInstances";

export const convertArtistDocToJs = async (doc: FirebaseFirestore.QueryDocumentSnapshot) => {
    const artist = doc.data();

    // 데뷔일 전환
    artist.debut = (artist.debut).toDate().toLocaleDateString();

    // 곡 목록 전환
    if (artist.songs) {
        artist.songs = await Promise.all((artist.songs).map(async (songDocRef: FirebaseFirestore.DocumentReference) => {
            const snapshot = await songDocRef.get();
            const songData = await snapshot.data();
            if (!songData) {
                throw new Error('song reference is not valid');
            }

            if (songData.youtubePublishAt) {
                songData.youtubePublishAt = (songData.youtubePublishAt).toDate().toLocaleDateString();
            }
            return songData;
        }));
    }

    return artist as IArtistJs;
};

const convertDateToFirebaseTimeStamp = (object: any, fieldName: string | string[]) => {
    if (Array.isArray(fieldName)) {
        fieldName.forEach((field) => {
            object[field] = admin.firestore.Timestamp.fromDate(new Date(object[field]));
        });
    } else {
        object[fieldName] = admin.firestore.Timestamp.fromDate(new Date(object[fieldName]));
    }
};

const convertYoutubeSongs = async (songs: ISong[]) => {
    return await Promise.all(songs.map(async (song) => {
        if (typeof song === 'string') {
            return await getYoutubeInfo(song);
        }
        return song;
    })) as ISong[];
};

const addYoutubeId = async (song: ISong) => {
    if (typeof song === 'string') {
        throw new Error('song must be converted');
    }
    await songsCollection.doc(song.youtubeId).set(song);
    return songsCollection.doc(song.youtubeId)
};

export const convertArtistJSToDoc = async (artist: IArtistJs) => {
    // 데뷔일 전환
    convertDateToFirebaseTimeStamp(artist, 'debut');
    // 곡목록 전환
    if (artist.songs) {
        artist.songs = await convertYoutubeSongs(artist.songs as ISong[]);
        artist.songs = await Promise.all((artist.songs as ISong[]).map(async (song) => {
            convertDateToFirebaseTimeStamp(song, 'youtubePublishAt');
            return await addYoutubeId(song);
        })) as FirebaseFirestore.DocumentReference[];
    }

    return artist;
};
