import {https} from 'firebase-functions';
import express from 'express';
import apiApp from './api';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const mainApp = express();
mainApp.use('/api', apiApp);

// @ts-ignore
// noinspection JSUnusedGlobalSymbols
export const main = https.onRequest(mainApp);
