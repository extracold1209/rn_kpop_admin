import React from 'react';
import ReactDom from 'react-dom';
import MainContainer from "./components/MainContainer";
import CssBaseline from '@material-ui/core/CssBaseline';
import {Provider} from "react-redux";
import store from './store';
import {refreshArtistList} from "./store/modules/dataReducer";

ReactDom.render(
    <>
        <Provider store={store}>
            <CssBaseline/>
            <MainContainer/>
        </Provider>
    </>,
    document.getElementById('__workspace__'),
    () => {
        store.dispatch(refreshArtistList())
    });
