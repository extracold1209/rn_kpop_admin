import {Action, combineReducers} from 'redux';
import statusReducer, {IStatusReducer} from './statusReducer';
import dataReducer, {IDataReducer} from './dataReducer';

export type IReduxAction<P, T> = (payload: P) => Action<T> & { payload: P }

export function makeAction<P, T>(type: T): IReduxAction<P, T> {
    return (payload: P) => ({type, payload});
}

export type IReducers = {
    status: IStatusReducer,
    data: IDataReducer,
}

export default combineReducers<IReducers>({
    status: statusReducer,
    data: dataReducer,
});
