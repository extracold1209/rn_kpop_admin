import {produce} from 'immer';
// Actions
import {Reducer} from 'redux';

// interfaces
export type IStatusReducer = {
    isNetworkBusy: boolean;
}

export const NETWORK_UPDATE = 'status/updating';

// Action Functions
export const setNetworkIsBusy = (isNetworkBusy: boolean) => ({type: NETWORK_UPDATE, payload: isNetworkBusy});

// Default State
const defaultState: IStatusReducer = {
    isNetworkBusy: false,
};

// Reducer
const statusReducer: Reducer<IStatusReducer> = (state = defaultState, action) => {
    switch (action.type) {
        case NETWORK_UPDATE:
            return produce<IStatusReducer>(state, (draft) => {
                draft.isNetworkBusy = action.payload;
            });
        default:
            return state;
    }
};

export default statusReducer;
