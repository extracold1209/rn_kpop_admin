// Actions
import {Reducer} from 'redux';
import produce from 'immer';

// interfaces
export type IArtist = {
    name: string;
    agency: string;
    members: string[];
    debut: string;
    twitterId?: string;
    vLiveId?: string;
    thumbnailImage: string;
    songs: ISong[]
    readonly docId?: string; // 히든값으로 보면된다. 리액트 내에서 사용되어서도, 변경되어서도 안된다.
}

export type ISong = string | {
    name: string;
    youtubeId: string;
    youtubeViewCount: number;
    youtubePublishAt: string;
    clickCount: number;
    playCount: number;
    thumbnailUrl: string;
}

export type IDataReducer = {
    artists: IArtist[],
    selectedArtist?: IArtist;
}

export const ARTIST_LIST_CHANGED = 'data/ARTIST_LIST_CHANGED';
export const ARTIST_SELECTED = 'data/ARTIST_SELECTED';
export const ARTIST_REFRESHED = 'data/ARTIST_REFRESHED';
export const ARTIST_UPDATE_REQUESTED = 'data/ARTIST_UPDATE_REQUESTED';
export const ARTIST_ADDED = 'data/ARTIST_ADDED';

// Action Functions
export const refreshArtistList = () => ({type: ARTIST_REFRESHED});
export const changeArtistList = (artist: IArtist[]) => ({type: ARTIST_LIST_CHANGED, payload: artist});
export const selectArtist = (artist: IArtist) => ({type: ARTIST_SELECTED, payload: artist});
export const updateArtist = (artist: IArtist) => ({type: ARTIST_UPDATE_REQUESTED, payload: artist});
export const insertArtist = (artist: IArtist) => ({type: ARTIST_ADDED, payload: artist});

// Default State
const defaultState: IDataReducer = {
    artists: [],
    selectedArtist: undefined,
};

// Reducer
const dataReducer: Reducer<IDataReducer> = (state = defaultState, action) => {
    switch (action.type) {
        case ARTIST_LIST_CHANGED:
            return produce(state, (draft) => {
                draft.artists = action.payload;
            });
        case ARTIST_SELECTED: {
            return produce(state, (draft) => {
                draft.selectedArtist = action.payload;
            });
        }
        default:
            return state;
    }
};

export default dataReducer;
