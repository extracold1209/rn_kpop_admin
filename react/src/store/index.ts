import {createStore, applyMiddleware} from 'redux';
import reducers from './modules';
import createSagaMiddleware from 'redux-saga';
import {helloSaga} from './sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(helloSaga);

export default store;
