import {call, put, takeEvery} from 'redux-saga/effects';
import fetch from 'unfetch';
import {
    ARTIST_ADDED,
    ARTIST_REFRESHED,
    ARTIST_UPDATE_REQUESTED,
    changeArtistList,
    IArtist,
    refreshArtistList
} from "../modules/dataReducer";
import {Action} from "redux";
import {setNetworkIsBusy} from "../modules/statusReducer";

function* handleRefreshArtists() {
    try {
        yield put(setNetworkIsBusy(true));
        const data = yield call(fetch, '/api/artists');
        const artists = yield call([data, data.json]);
        yield put(changeArtistList(artists));
        yield put(setNetworkIsBusy(false));
    } catch (e) {
        alert('목록 불러오기가 실패했습니다.');
        console.error(e);
    }
}

function* handleUpdateArtist(action: Action & { payload: IArtist }) {
    const artist = action.payload;
    try {
        yield put(setNetworkIsBusy(true));
        yield call(fetch, '/api/artists', {
            headers: {'Content-Type': 'application/json'},
            method: 'PUT',
            body: JSON.stringify(artist)
        });
        alert('업데이트가 완료되었습니다.');
        yield put(refreshArtistList());
    } catch (e) {
        alert('업데이트가 실패하였습니다.');
        console.error(e);
    } finally {
        yield put(setNetworkIsBusy(false))
    }
}

function* handleAddArtist(action: Action & { payload: IArtist }) {
    const artist = action.payload;
    try {
        yield put(setNetworkIsBusy(true));
        yield call(fetch, '/api/artists', {
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            body: JSON.stringify(artist)
        });
        alert('추가가 완료되었습니다.');
        yield put(refreshArtistList());
    } catch (e) {
        alert('추가가 실패하였습니다.');
        console.error(e);
    } finally {
        yield put(setNetworkIsBusy(false))
    }

}

export function* helloSaga() {
    yield takeEvery(ARTIST_REFRESHED, handleRefreshArtists);
    yield takeEvery(ARTIST_UPDATE_REQUESTED, handleUpdateArtist);
    yield takeEvery(ARTIST_ADDED, handleAddArtist);
}
