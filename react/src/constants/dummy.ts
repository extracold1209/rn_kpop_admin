export const dummyBangTan = {
    name: '방탄소년단',
    agency: 'Big Hit',
    twitterId: 'BTS_twt',
    vLiveId: 'FE619',
    members: ['SUGA', 'Jin', 'J−HOPE', 'RM', 'Jimin', 'V', 'Jungkook'],
    debut: '2010-09-30',
    thumbnailImage: 'https://w.namu.la/s/100c981d8225e30d65ae00507e7e44468f3c6979742a102f646838c2eba4dc074c4e1d637f3d3399bf9a97e0adcdf35833fcfb05c5eaf5905b3bf609ef60cd2becb08e76d7077064ff7dcfe76bd7036cbcdb9b0246e8b37e4285c7e0d9295f1c7f48e845c7bc388aaad9dd94a1dfe9a2',
    songs: [],
};
