import React, {useEffect, useRef} from 'react';
import { cloneDeep } from 'lodash-es';
import TextField from "@material-ui/core/TextField";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {dummyBangTan} from "../constants/dummy";
import {TextFieldProps} from "@material-ui/core/TextField/TextField";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textField: {
            display: 'flex',
            marginBottom: theme.spacing(1),
            marginTop: theme.spacing(1),
        },
    }),
);

type IProps = {
    data?: any;
    disabled: boolean;
    onTextChange?: (obj: any) => void;
}


const ArtistSectionInfo: React.FC<IProps> = (props) => {
    const classes = useStyles();
    const artistRef = useRef(null);
    const {disabled} = props;

    return (
        <>
            <TextField size={"small"} id="outlined-basic" label="아티스트명" variant="outlined"
                       ref={artistRef}
                       fullWidth
                       disabled={disabled}
                       className={classes.textField}/>
            <TextField size={"small"} id="outlined-basic" label="소속사" variant="outlined"
                       fullWidth
                       disabled={disabled}
                       className={classes.textField}/>
            <TextField size={"small"} id="outlined-basic" label="데뷔일" variant="outlined"
                       fullWidth
                       disabled={disabled}
                       className={classes.textField}/>
            <TextField size={"small"} id="outlined-basic" label="멤버들" variant="outlined"
                       fullWidth
                       disabled={disabled}
                       className={classes.textField}/>
        </>
    )
};

export default ArtistSectionInfo;
