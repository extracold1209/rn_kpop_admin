import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React, {ReactElement} from "react";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import {AppTitle} from "../constants";

const ElevationScroll: React.FC<{ window?: () => Window }> = (props) => {
    const {children, window} = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined,
    });

    return React.cloneElement(children as ReactElement, {
        elevation: trigger ? 4 : 0,
    });
};

const ScrollableAppBar: React.FC = () => (
    <>
        <ElevationScroll>
            <AppBar>
                <Toolbar>
                    <Typography variant="h6">{AppTitle}</Typography>
                </Toolbar>
            </AppBar>
        </ElevationScroll>
        <Toolbar style={{marginBottom: 8}}/> {/* just styling */}
    </>
);

export default ScrollableAppBar;
