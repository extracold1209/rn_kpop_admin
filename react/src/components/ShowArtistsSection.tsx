import React from 'react';
import Box from '@material-ui/core/Box';
import ArtistCard from './ArtistCard';
import {useDispatch, useSelector} from "react-redux";
import {IReducers} from "../store/modules";
import {IArtist, updateArtist} from "../store/modules/dataReducer";
import {CircularProgress, createStyles} from "@material-ui/core";
import {makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
        }
    }),
);

const ShowArtistsSection: React.FC = () => {
    const classes = useStyles();
    const artists = useSelector<IReducers, IArtist[]>((state) => state.data.artists);
    const dispatch = useDispatch();

    return (
        <Box className={classes.root}>
            {artists.length === 0
                ? <CircularProgress/>
                : artists.map((artist) => (
                    <ArtistCard
                        key={artist.name}
                        data={artist}
                        onSave={(result) => {
                            dispatch(updateArtist(result as IArtist))
                        }}
                    />
                ))
            }
        </Box>
    );
};

export default ShowArtistsSection;
