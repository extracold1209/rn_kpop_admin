import React, {forwardRef, useImperativeHandle, useMemo, useRef, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {reduce} from 'lodash';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import TextField from "@material-ui/core/TextField";
import ChipInput from 'material-ui-chip-input';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import {IArtist, ISong} from "../store/modules/dataReducer";
import {useSelector} from "react-redux";
import {IReducers} from "../store/modules";

type IProps = {
    data?: IArtist;
    onSave?: (result: Partial<IArtist>) => void;
    onDelete?: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            width: '100%',
            marginBottom: theme.spacing(2),
        },
        content: {
            display: 'flex',
            flex: '1 0 auto',
            flexDirection: 'column',
            justifyContent: 'center'
        },
        imageContainer: {
            minHeight: 200
        },
        cover: {
            width: '100%',
            height: '100%',
        },
        textField: {
            display: 'flex',
            width: '100%',
            marginBottom: theme.spacing(1),
            marginTop: theme.spacing(1),
        },
        miniTextField: {
            marginRight: theme.spacing(1),
        },
        editContainer: {
            padding: theme.spacing(2),
            paddingTop: theme.spacing(1),
        },
        buttonContainer: {
            display: 'inherit',
            justifyContent: 'flex-end',
        },
        button: {
            height: '100%'
        }
    }),
);
const ArtistCard = forwardRef<{ reset(): void }, IProps>((props, ref) => {
    const isNetworkBusy = useSelector<IReducers, boolean>((state) => state.status.isNetworkBusy);
    const classes = useStyles();
    const {data} = props;
    const [resetTrigger, reset] = useState(false);

    let currentResult: Partial<IArtist> = useMemo(() => Object.assign({}, data), [data, resetTrigger]);
    const {name, agency, members, thumbnailImage, debut, songs, twitterId, vLiveId} = currentResult;

    const [membersState, setMembers] = useState<string[] | undefined>(members);
    const [songsState, setSongs] = useState<ISong[] | undefined>(songs);

    const artistRef = useRef<HTMLInputElement>(null);
    const agencyRef = useRef<HTMLInputElement>(null);
    const debutRef = useRef<HTMLInputElement>(null);
    const thumbnailRef = useRef<HTMLInputElement>(null);
    const twitterRef = useRef<HTMLInputElement>(null);
    const vLiveRef = useRef<HTMLInputElement>(null);

    useImperativeHandle(ref, () => ({
        reset() {
            const confirmed = confirm('전부 지우시겠습니까?');
            if (confirmed) {
                artistRef.current && (artistRef.current.value = '');
                agencyRef.current && (agencyRef.current.value = '');
                debutRef.current && (debutRef.current.value = '');
                thumbnailRef.current && (thumbnailRef.current.value = '');
                twitterRef.current && (twitterRef.current.value = '');
                vLiveRef.current && (vLiveRef.current.value = '');
                setMembers([]);
                setSongs([]);
                currentResult = reduce(currentResult, (result, value, key) => {
                    if (Array.isArray(value)) {
                        result[key] = [];
                    } else {
                        result[key] = '';
                    }
                    return result;
                }, {} as any);

                reset(!resetTrigger);
            }
        }
    }));

    return (
        <Card className={classes.root}>
            <Grid container spacing={1}>
                {
                    thumbnailImage && (
                        <Grid item xs={12} sm={3} className={classes.imageContainer}>
                            <CardMedia
                                className={classes.cover}
                                image={thumbnailImage}
                                title={name}
                            />
                        </Grid>
                    )
                }
                <Grid item xs={12} sm={thumbnailImage ? 9 : 12}>
                    <CardContent className={classes.content}>
                        <TextField size={"small"} label="아티스트명" variant="outlined"
                                   defaultValue={name}
                                   inputRef={artistRef}
                                   onChange={(value) => {
                                       currentResult.name = value.target.value
                                   }}
                                   className={classes.textField}/>
                        <TextField size={"small"} label="소속사" variant="outlined"
                                   defaultValue={agency}
                                   inputRef={agencyRef}
                                   onChange={(value) => {
                                       currentResult.agency = value.target.value
                                   }}
                                   className={classes.textField}/>
                        <TextField size={"small"} label="데뷔일" variant="outlined"
                                   defaultValue={debut}
                                   inputRef={debutRef}
                                   onChange={(value) => {
                                       currentResult.debut = value.target.value
                                   }}
                                   className={classes.textField}/>
                        <TextField size={"small"} label="섬네일주소" variant="outlined"
                                   defaultValue={thumbnailImage}
                                   inputRef={thumbnailRef}
                                   onChange={(value) => {
                                       currentResult.thumbnailImage = value.target.value
                                   }}
                                   className={classes.textField}/>
                        <ChipInput size={"small"} label="멤버들" variant="outlined"
                                   value={membersState || []}
                                   onAdd={(chip) => {
                                       const nextState = [...(membersState || []), chip];
                                       currentResult.members = nextState;
                                       setMembers(nextState);
                                   }}
                                   onDelete={(deletedChip) => {
                                       const nextState = (membersState || []).filter((member) => member !== deletedChip);
                                       currentResult.members = nextState;
                                       setMembers(nextState);
                                   }}
                                   className={classes.textField}
                        />
                        <ChipInput
                            label={'영상목록'}
                            size={'small'}
                            value={songsState?.map((song) => (typeof song === 'string' ? song : song.youtubeId)) || []}
                            onClick={(chip) => {
                                // @ts-ignore
                                if (chip.target.className === 'MuiChip-label' && chip.target.textContent) {
                                    // @ts-ignore
                                    window.open(`https://www.youtube.com/watch?v=${chip.target.textContent}`);
                                }
                            }}
                            onAdd={(chip) => {
                                const nextState = [...(songsState || []), chip];
                                currentResult.songs = nextState;
                                console.log('onAdd', currentResult);
                                setSongs(nextState);
                            }}
                            onDelete={(deletedChip) => {
                                const nextState = (songsState || []).filter((song) => {
                                    if (typeof song === 'string') {
                                        return song !== deletedChip;
                                    } else {
                                        return song.youtubeId !== deletedChip;
                                    }
                                });
                                currentResult.songs = nextState;
                                console.log('onDelete', currentResult);
                                setSongs(nextState);
                            }}
                            variant={'outlined'}
                            className={classes.textField}
                        />
                    </CardContent>
                    <CardActions className={classes.editContainer}>
                        <Grid container>
                            <Grid item xs={12} sm={10}>
                                <TextField variant={'outlined'} size={'small'} label={'Twitter ID'}
                                           className={classes.miniTextField} defaultValue={twitterId}
                                           inputRef={twitterRef}
                                           onChange={(value) => {
                                               currentResult.twitterId = value.target.value;
                                           }}/>
                                <TextField variant={'outlined'} size={'small'} label={'V-Live ID'}
                                           inputRef={vLiveRef}
                                           className={classes.miniTextField} defaultValue={vLiveId}
                                           onChange={(value) => {
                                               currentResult.vLiveId = value.target.value;
                                           }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={2} className={classes.buttonContainer}>
                                <Button variant={'contained'} color={'primary'}
                                        disabled={isNetworkBusy}
                                        onClick={() => props.onSave && props.onSave(Object.assign(currentResult, {
                                            members: membersState,
                                            songs: songsState
                                        }))}
                                        className={classes.miniTextField}>저장</Button>
                                <Button variant={'contained'} color={'secondary'}
                                        disabled={isNetworkBusy}
                                        onClick={() => props.onDelete && props.onDelete()}>삭제</Button>
                            </Grid>
                        </Grid>
                    </CardActions>
                </Grid>
            </Grid>
        </Card>
    );
});

export default ArtistCard;
