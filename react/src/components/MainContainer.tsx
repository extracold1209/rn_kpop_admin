import React from 'react';
import Container from '@material-ui/core/Container';
import ScrollableAppBar from "./ScrollableAppBar";
import AddArtistSection from "./AddArtistSection";
import ShowArtistsSection from "./ShowArtistsSection";
import {useSelector} from "react-redux";
import {IReducers} from "../store/modules";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {createStyles} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        curtain: {
            position: 'fixed',
            width: '100%',
            height: '-webkit-fill-available',
            backgroundColor: 'grey',
            opacity: 0.4,
        }
    }),
);

export default function MainContainer() {
    const styles = useStyles();
    const isNetworkBusy = useSelector<IReducers, boolean>((state) => state.status.isNetworkBusy);

    return (
        <>
            {isNetworkBusy && <div className={styles.curtain}/>}
            <ScrollableAppBar/>
            <Container>
                <AddArtistSection/>
                <ShowArtistsSection/>
            </Container>
        </>
    );
}
