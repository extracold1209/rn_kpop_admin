import React, {useRef} from 'react';
import Box from "@material-ui/core/Box";
import ArtistCard from "./ArtistCard";
import {IArtist, insertArtist} from "../store/modules/dataReducer";
import {useDispatch} from "react-redux";

const AddArtistSection: React.FC = () => {
    const dispatch = useDispatch();
    const cardRef = useRef<any>(null);

    return (
        <>
            <Box p={1} mb={1} borderColor={'black'} borderRadius={4} border={1}>
                <ArtistCard
                    ref={cardRef}
                    onSave={(artist) => {
                        dispatch(insertArtist(artist as IArtist));
                    }}
                    onDelete={() => {
                        cardRef.current?.reset();
                    }}
                />
            </Box>
        </>
    );
};

export default AddArtistSection;
